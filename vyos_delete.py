
from netmiko import ConnectHandler
import datetime
def vyosDelete(connect):
    config_commands=["delete interfaces loopback lo",
    "delete protocols ospf",
    "delete policy",
    "commit","save","exit"]
    output = connect.send_config_set(config_commands)    
    print(output)

from  Package.Parser import *
vyos = Parser()

try:
    net_connect = ConnectHandler(**vyos)
    output = net_connect.send_command('show configuration')
    vyosDelete(net_connect)
    with open("Delete_Backup/config_backup_{}.txt".format(datetime.datetime.now()),'w') as file:
        file.write(output)

except Exception as e:
    print(e)
    print("Router not Working")