from netmiko import ConnectHandler
from Package.Vyos_config import * 
import datetime
from  Package.Parser import *
vyos = Parser()
flag="n"
try:
    net_connect = ConnectHandler(**vyos)
    output = net_connect.send_command('show configuration')
    print("Connection is Created successfully")
    flag=input("Configure router?(Y/N)")
except Exception as e:
    print(e)
    print("Router not Working or check the credentials and other settings")
if flag=='Y' or flag=='y':
    with open("Modify_Backup/config_backup_{}.txt".format(datetime.datetime.now()),'w') as file:
        file.write(output)
    try:
        vyosConfig(net_connect)
    except Exception as e:
        print(e)