def vyosConfig(connect):
    
    config_commands = [ "set interfaces loopback lo address 1.1.1.1/32", 
    "set protocols ospf area 0 network 192.168.0.0/24",
    "set protocols ospf default-information originate always",
    "set protocols ospf default-information originate metric 10",
    "set protocols ospf default-information originate metric-type 2",
    "set protocols ospf log-adjacency-changes",
    "set protocols ospf parameters router-id 1.1.1.1",
    "set protocols ospf redistribute connected metric-type 2",
    "set protocols ospf redistribute connected route-map CONNECT",
    "set policy route-map CONNECT rule 10 action permit",
    "set policy route-map CONNECT rule 10 match interface lo",
    "commit","save","exit"]
    output = connect.send_config_set(config_commands)    
    print(output)